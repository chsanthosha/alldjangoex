from django.shortcuts import render
from django.views.generic import View
import io
from rest_framework.parsers import JSONParser
# Create your views here.
class EmployeeCRUDCBV(View):
    def get(self,request,*args,**kwargs):
        json_data=request.body
        stream=io.Bytes(json_data)
        pdata=JSONParser().parse(stream)

