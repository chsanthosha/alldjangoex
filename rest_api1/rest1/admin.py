from django.contrib import admin
from rest1.models import Employee


# Register your models here.

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['eno', 'ename', 'esal']


admin.site.register(Employee, EmployeeAdmin)
