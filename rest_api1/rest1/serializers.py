from rest_framework import serializers

class EmployeeSerializer(serializers.Serializer):
    eno = serializers.IntegerField()
    ename = serializers.CharField()
    esal = serializers.FloatField()
   # add = serializers.CharField(max_length=64)