from django.db import models

from django.contrib.auth.models import AbstractBaseUser,BaseUserManager,PermissionsMixin,AbstractUser

from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from .managers import CustomUserManager
from django.conf import settings
from django.contrib.auth.models import User





class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(max_length=32)
    phone = models.CharField(max_length=16)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    account_type = models.CharField(max_length=255,null=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name','phone']
    """
    objects = CustomUserManager()
    """
    def __str__(self):
        return self.email




#class AgentUser(models.Model):
    #role = models.ManyToManyField(CustomUser)



"""
class Customer(AbstractBaseUser):

    name=models.CharField(max_length=32)
    phone=models.CharField(max_length=16)
    email=models.EmailField(max_length=40)
    role=models.CharField(max_length=16)

    USERNAME_FIELD=['email']
    REQUIRED_FIELDS = ['name','phone','role','status']

    def __str__(self):
        return self.email
"""







# Create your models here.
class flight(models.Model):

    apiname=models.CharField(max_length=32)
    markup=models.CharField(max_length=32)
    status=models.CharField(max_length=16)

class hotel(models.Model):

    apiname = models.CharField(max_length=32)
    hotelpnr = models.CharField(max_length=32)
    status = models.CharField(max_length=16)

class userflight(models.Model):
    name = models.CharField(max_length=32)
    bookingid=models.CharField(max_length=32)
    contact= models.IntegerField()
    status = models.CharField(max_length=16)

class userhotel(models.Model):
    name = models.CharField(max_length=32)
    bookingid = models.CharField(max_length=32)
    contact = models.IntegerField()
    hotelpnr=models.CharField(max_length=32)
    status = models.CharField(max_length=16)



