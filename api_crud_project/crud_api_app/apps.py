from django.apps import AppConfig


class CrudApiAppConfig(AppConfig):
    name = 'crud_api_app'
