from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.renderers import  JSONRenderer
from django.http import HttpResponse
import io
from rest_framework.generics import ListAPIView,CreateAPIView,RetrieveAPIView,UpdateAPIView,DestroyAPIView,RetrieveUpdateAPIView
# Create your views here.

from rest_framework import mixins, status, generics
from .models import flight,hotel,userflight,userhotel,CustomUser
from .serializers import flightserializer,hotelserializer,userflightserializer,userhotelserializer,Customerserializer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view,permission_classes



class CustomerCreateAPIView(generics.CreateAPIView):
    def post(self, request, *args, **kwargs):


        json_data = request.body
        print( json_data)
        stream = io.BytesIO(json_data)
        pdata = JSONParser().parse(stream)  # converting json data into python data
        print(pdata)
        dp=pdata
        x=dp['role']
        print(x)
        c='Customer'
        a='PackageAgent'
        if x==c:
            serializer = Customerserializer(data=pdata)

            print(serializer)
            if serializer.is_valid():  # save db
                    print('serializer valid')
                    user = serializer.save()
                    print(user)
                    # msg={'msg':'Resource created successfully'} # this is the python native
                    # json_data=JSONRenderer().render(msg)  #converting python native to json
                    print(json_data)
                    stream = io.BytesIO(json_data)
                    resp = JSONParser().parse(stream)
                    print(resp)

                    return HttpResponse(resp, content_type='application/json', status=status.HTTP_201_CREATED)

        print('beforeserializer error')
        json_data = JSONRenderer().render(serializer.errors)
        print(json_data)
        return HttpResponse(json_data, content_type='application/json', status=400)


class flightcreateAPIView(CreateAPIView):
    queryset =flight.objects.all()
    serializer_class = flightserializer

class flightListAPIView(ListAPIView):
    queryset =flight.objects.all()
    serializer_class = flightserializer

class hotelListAPIView(ListAPIView):
    queryset =hotel.objects.all()
    serializer_class = hotelserializer

    def get_queryset(self):
        qs=hotel.objects.all()
        pnr=self.request.GET.get('hotelpnr')
        if pnr is not None:
            qs=qs.filter(hotelpnr__icontains=pnr)
        return qs


class hotelcreateAPIView(CreateAPIView):
    queryset =hotel.objects.all()
    serializer_class = hotelserializer


class hotelRetrieveAPIView(RetrieveAPIView):
    queryset=hotel.objects.all()
    serializer_class = hotelserializer
    lookup_field = 'id'


class hotelDestroyAPIView(DestroyAPIView):
    queryset=hotel.objects.all()
    serializer_class = hotelserializer
    lookup_field = 'id'


class userflightcreateAPIView(CreateAPIView):
    queryset =userflight.objects.all()
    serializer_class = userflightserializer


class userhotelcreateAPIView(CreateAPIView):
    queryset =userhotel.objects.all()
    serializer_class = userhotelserializer




























"""
class EmployeeListCreateAPIView(mixins.CreateModelMixin,ListAPIView):
    queryset=Employee.objects.all()
    serializer_class=EmployeeSerializer
    def post(self,request,*args,**kwargs):
        return self.create(request,*args,**kwargs)





"""























































""""
class EmployeeRetrieveAPIView(RetrieveAPIView):
    queryset=Employee.objects.all()
    serializer_class = EmployeeSerializer
    lookup_field = 'pk'

class EmployeeUpdateAPIView(UpdateAPIView):
    queryset=Employee.objects.all()
    serializer_class=EmployeeSerializer
    lookup_field = 'pk'



class EmployeeCreateAPIView(CreateAPIView):
    queryset=Employee.objects.all()
    serializer_class = EmployeeSerializer


class EmployeeDestroyAPIView(DestroyAPIView):
    queryset=Employee.objects.all()
    serializer_class = EmployeeSerializer


class EmployeeRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    queryset=Employee.objects.all()
    serializer_class = EmployeeSerializer
    lookup_field = 'pk'



class EmployeeListAPIView(ListAPIView):
        #queryset = Employee.objects.all() #query set predefined class ,,if u need customization need to ovrride queryset
        serializer_class = EmployeeSerializer # serializer class predefined

        # to implement search opeartion am overriding this method.
        def get_queryset(self):
            qs=Employee.objects.all()
            name=self.request.GET.get('eno')
            if name is not None:
                qs=qs.filter(ename__icontains=name)
            return qs




class EmployeeListAPIView(APIView):
    def get(self,request,format=None):
        qs=Employee.objects.all()
        serializer=EmployeeSerializer(qs,many=True)
        return Response(serializer.data)
        
#to convert python native to json data
#dict data is available serializer.data
#Response class Automatically convert python native to json """