


	import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleService } from '../../role/role.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SignupService } from '../../../../login-signup/signup/signup.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from '../../../../common/constants.enum';
import { AdminUsersService } from '../admin-users.service';

@Component({
  selector: 'app-admin-users-form',
  templateUrl: './admin-users-form.component.html',
  styleUrls: ['./admin-users-form.component.scss'],
  providers: [RoleService, SignupService, AdminUsersService]

})
export class AdminUsersFormComponent implements OnInit {
  roles;
  currencies = ['INR', 'USD', 'CNY'];
  adminUserForm: FormGroup;
  adminStatus;
  status = ['active', 'inactive'];
  constructor(private router: Router, private roleService: RoleService,
              private formBuilder: FormBuilder, private signupService: SignupService,
              private activeRoute: ActivatedRoute, private adminUserService: AdminUsersService) { }

  ngOnInit() {
    this.adminUserForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', [Validators.required, Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      transactionPassword: ['', Validators.required],
      role: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      alternateEmail: ['', Validators.required],
      alternateMobileNo: ['', Validators.required],
      city: ['', Validators.required],
      agencyName: ['', Validators.required],
      postCode: ['', Validators.required],
      telephoneNo: ['', Validators.required],
      currency: ['', Validators.required],
      panNo: ['', Validators.required],
      services: ['', Validators.required],
      address: ['', Validators.required],
      status: ['', Validators.required]
    });
    this.adminUserForm.patchValue({
      currency: this.currencies[0]
    });
    this.roleService.getAllRoles().subscribe(data => {
      if (data.success === '1') {
        this.roles = data.data;
        this.adminUserForm.patchValue({
          role: this.roles[0].role
        });
      }
    });
    localStorage.getItem('adminStatus') === 'adminEdit' ? this.adminStatus = true : this.adminStatus = false;
    this.adminUserService.getSingleUser(this.activeRoute.snapshot.params.id).subscribe(data => {
      if (data.success === '1') {
        this.adminUserForm.patchValue({
          firstName: data.data.firstName,
          lastName: data.data.lastName,
          phone: data.data.phone,
          email: data.data.email,
          password: this.decrypt(data.data.password),
          transactionPassword: data.data.transactionPassword,
          role: data.data.role,
          gender: data.data.gender,
          dob: data.data.dob,
          alternateEmail: data.data.alternateEmail,
          alternateMobileNo: data.data.alternateMobileNo,
          city: data.data.city,
          agencyName: data.data.agencyName,
          postCode: data.data.postCode,
          telephoneNo: data.data.telephoneNo,
          currency: data.data.currency,
          panNo: data.data.panNo,
          services: data.data.services,
          address: data.data.address,
          status: data.data.status
        });
      }
    });
  }

  onAdminUserSave() {
    const encryptedPswd = this.encrypt(this.adminUserForm.value.password);
    const encryptedTransactionPswd = this.encrypt(this.adminUserForm.value.transactionPassword);
    this.adminUserForm.value.password = encryptedPswd;
    this.adminUserForm.value.transactionPassword = encryptedTransactionPswd;
    console.log(this.adminUserForm.value);
    this.signupService.signup(this.adminUserForm.value).subscribe(data => {
      if (data.success === '1') {
        this.router.navigateByUrl('admindashboard/adminusers');
      }
    });
  }

  onAdminUserUpdate() {
    this.adminUserService.updateUser(this.adminUserForm.value, this.activeRoute.snapshot.params.id).subscribe(data => {
      if (data.success === '1') {
        this.router.navigateByUrl('admindashboard/adminusers');
      }
    });
  }

  encrypt(data: any) {
    const encrypted = CryptoJS.AES.encrypt(data, Constants.CRYPTO_KEY).toString();
    return encrypted;
  }
  decrypt(data: any) {
    const decrypted = CryptoJS.AES.decrypt(data, Constants.CRYPTO_KEY).toString();
    return decrypted;
  }

  onAdminUserCancel() {
    this.router.navigateByUrl('admindashboard/adminusers');
  }

}


