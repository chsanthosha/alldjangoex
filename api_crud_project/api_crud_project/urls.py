"""api_crud_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from crud_api_app import views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('flightcreate/',views.flightcreateAPIView.as_view()),
    path('hotelcreate/',views.hotelcreateAPIView.as_view()),
    path('userflightcreate/', views.userflightcreateAPIView.as_view()),
    path('userhotelcreate/', views.userhotelcreateAPIView.as_view()),
    path('flightlist/',views.flightListAPIView.as_view()),
    path('hotellist/', views.hotelListAPIView.as_view()),
    path('hotelret/<int:id>/',views.hotelRetrieveAPIView.as_view()),

    path('hoteldestroy/<int:id>/', views.hotelDestroyAPIView.as_view()),
    path('usercreate/',views.CustomerCreateAPIView.as_view()),








    # path('api/',views.EmployeeListCreateModelMixin.as_view())
   # path('api/',views.EmployeeListAPIView.as_view())
    #path('api/<int:pk>/', views.EmployeeRetrieveAPIView.as_view()),
   # path('api/<int:pk>/', views.EmployeeUpdateAPIView.as_view())
   # path('api/<int:pk>/', views.EmployeeDestroyAPIView.as_view()),
   # path('api/<int:pk>/', views.EmployeeRetrieveUpdateAPIView.as_view()),

]
