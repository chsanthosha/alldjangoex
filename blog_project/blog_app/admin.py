from django.contrib import admin
from blog_app.models import Post

# Register your models here.
    #admin.site.register(Post)

class PostAdmin(admin.ModelAdmin):
    list_display = ['title','slug','author','body','publish','created','updated','status']
    list_filter = ('status','created','author','publish',)
    search_fields = ('title','body',)
    raw_id_fields = ('author',)
    date_hierarchy = 'publish'
    ordering = ['status','publish']
    prepopulated_fields = {'slug':('title',)}
#prepopulated_fields meaning based on some other fields
admin.site.register(Post,PostAdmin)