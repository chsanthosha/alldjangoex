# Generated by Django 2.2.4 on 2020-04-01 11:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sessionapp', '0002_b2cuser'),
    ]

    operations = [
        migrations.AddField(
            model_name='b2buser',
            name='agentemail',
            field=models.CharField(max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='b2buser',
            name='agentfirstname',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AddField(
            model_name='b2buser',
            name='agentlastname',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AddField(
            model_name='b2buser',
            name='agentphonenumber',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='b2cuser',
            name='email',
            field=models.CharField(max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='b2cuser',
            name='firstname',
            field=models.CharField(max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='b2cuser',
            name='lastname',
            field=models.CharField(max_length=256, null=True),
        ),
        migrations.AddField(
            model_name='b2cuser',
            name='phonenumber',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='b2buser',
            name='companyname',
            field=models.CharField(max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='b2cuser',
            name='status',
            field=models.CharField(max_length=256, null=True),
        ),
    ]
