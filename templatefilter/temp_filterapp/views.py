from django.shortcuts import render
from temp_filterapp.models import FilterModel
# Create your views here.

def Upper_view(request):
    data_list=FilterModel.objects.all()
    return render(request, 'temp_filterapp/upper.html', {'data_list':data_list})