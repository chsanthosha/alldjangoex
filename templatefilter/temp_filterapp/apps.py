from django.apps import AppConfig


class TempFilterConfig(AppConfig):
    name = 'temp_filterapp'
