from django.apps import AppConfig


class RestAuthAppConfig(AppConfig):
    name = 'rest_auth_app'
