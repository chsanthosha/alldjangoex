from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import BaseUserManager,AbstractBaseUser

USERNAME_REGEX='^[a-zA-Z0-9,+-]*$'

class MyUsermanager(BaseUserManager):
    def create_user(self,username,email,password=None):
        if not email:
            raise ValueError('user must have email address')
        user=self.model(
            username=username,
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self.db)
        return user
    def create_superuser(self,username,email,password=None):
        user=self.create_user(username,email,password=password)
        user.is_admin=True
        user.save(using=self.db)
        return user

class MyUser(AbstractBaseUser):
    username=models.CharField(max_length=255,validators=[RegexValidator(regex=USERNAME_REGEX,
                                                         message='username must be alphanumeeric',
                                                         code='ivalid_username')],unique=True)
    email=models.EmailField(max_length=255,unique=True,verbose_name='email address')
    is_staff=models.BooleanField(default=False)
    is_admin=models.BooleanField(default=False)
    objects=MyUsermanager()
    USERNAME_FIELD='username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True



