from rest_framework import serializers
from crudapi_app.models import Employee

def multiples_of_1000(value):
    print("came into multiples of thousand validation")
    if value %1000 !=0:
        raise serializers.ValidationError('employee salary should be mutiples of 1000')
# model by default provide create and update method
class EmployeeSerializer(serializers.ModelSerializer):
    eno=serializers.IntegerField(validators=[multiples_of_1000])
    class Meta:
        model=Employee
        fields='__all__'


"""

class EmployeeSerializer(serializers.Serializer):
    eno=serializers.IntegerField(validators=[multiples_of_1000])
    ename=serializers.CharField(max_length=64)
    eaddr=serializers.CharField(max_length=64)

    def validate_eno(self, value):
        print("field level  validation is going to happen first")
        if value < 100:
            raise serializers.ValidationError('Employee Salary Should min 100')
        return value
    def validate(self, data):
        print('object level validadation')
        #if anywhere failure is there updation is not going to update
        eno=data.get('eno')
        ename=data.get('ename')
        if ename =='devarsh':
            if eno<150:
                raise  serializers.ValidationError('number should be min 150')
        return data

    #if you are creating table
    def create(self,validated_data):
        print("working with create function in serializer")
        return Employee.objects.create(**validated_data) # for the Post Operations
    def update(self,instance,validated_data):  #validated_data metho is fixed existing object is instance
        instance.eno=validated_data.get('eno',instance.eno)
        instance.ename=validated_data.get('ename',instance.ename)
        instance.eaddr=validated_data.get('eaddr',instance.eaddr)
        instance.save()
        return instance
        
"""

