from django.apps import AppConfig


class CrudapiAppConfig(AppConfig):
    name = 'crudapi_app'
