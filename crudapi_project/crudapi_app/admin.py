from django.contrib import admin
from crudapi_app.models import Employee

# Register your models here.
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['id','eno','ename','eaddr']


admin.site.register(Employee,EmployeeAdmin)