from django.shortcuts import render
from django.views.generic import View
import io
from rest_framework.parsers import JSONParser
from crudapi_app.serializers import EmployeeSerializer
from rest_framework.renderers import  JSONRenderer
from .models import Employee
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

# Create your views here.

@method_decorator(csrf_exempt,name='dispatch')

class EmployeeCRUDCBV(View): # which is the child class of view belongs to django.view.generic,,,#here we using View it is from django but not rest framework



    def get(self,request,*args,**kwargs):
        print(request.body)
        json_data=request.body # have to convert the json data into python native types
        print(json_data)
        #convert into stream
        stream=io.BytesIO(json_data)
        print(stream)
        pdata=JSONParser().parse(stream)
        print(pdata)
        id=pdata.get('id',None) # if it is id get id related content otherwise return None
        if id is not None:
            emp=Employee.objects.get(id=id)
            print(emp)
            serializer=EmployeeSerializer(emp)
            print(serializer)
            json_data=JSONRenderer().render(serializer.data)
            print(json_data)
            return HttpResponse(json_data,content_type='application/json')

        qs=Employee.objects.all()
        serializer=EmployeeSerializer(qs,many=True)
        json_data=JSONRenderer().render(serializer.data)
        print(type(json_data))
        return HttpResponse(json_data,content_type='application/json')
    def post(self,request,*args,**kwargs):
        print(request.body)
        json_data=request.body
        print('1',json_data)
        stream=io.BytesIO(json_data)

        pdata=JSONParser().parse(stream) #converting json data into python data
        serializer=EmployeeSerializer(data=pdata) #converting python native data to database suppoted data Desrializer
        if serializer.is_valid(): # save db
            serializer.save()
            msg={'msg':'Resource created successfully'} # this is the python native
            json_data=JSONRenderer().render(msg)  #converting python native to json
            return HttpResponse(json_data,content_type='application/json')

        json_data=JSONRenderer().render(serializer.errors)
        return HttpResponse(json_data,content_type='application/json',status=400)
    def put(self,request,*args,**kwargs):
        json_data=request.body # convert json data into python data
        stream=io.BytesIO(json_data)
        pdata=JSONParser().parse(stream)
        id=pdata.get('id')
        print(id)
        emp=Employee.objects.get(id=id)
        print(emp)
        print(pdata)
        serializer=EmployeeSerializer(emp,data=pdata)#create serializer object padat=provided data by partner application
       # serializer=EmployeeSerializer(emp,data=pdata,partial=True)#create serializer object padat=provided data by partner application

        if serializer.is_valid():
            serializer.save()
            msg={"resourcses updated successfully"}

            json_data=JSONRenderer().render(msg)
            return  HttpResponse(json_data,content_type='application/json')
        json_data=JSONRenderer().render(serializer.errors)
        return HttpResponse(json_data,content_type='application/json',status=400)
    def delete(self, request,*args,**kwargs):
        json_data=request.body
        stream=io.BytesIO(json_data)
        pdata=JSONParser().parse(stream)
        id=pdata.get('id')
        emp=Employee.objects.get(id=id)
        emp.delete()
        msg={"resource deleted successfully"}
        json_data=JSONRenderer().render(msg)
        return HttpResponse(json_data,content_type='application/json')
