from django.apps import AppConfig


class CrudAuthTokenAppConfig(AppConfig):
    name = 'crud_auth_token_app'
