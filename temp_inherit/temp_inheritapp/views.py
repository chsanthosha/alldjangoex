from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
def home_view(request):
    return render(request ,'temp_inheritapp/home.html')

def movies_view(request):
    return render(request ,'temp_inheritapp/movies.html')


def sports_view(request):
    return render(request ,'temp_inheritapp/sports.html')