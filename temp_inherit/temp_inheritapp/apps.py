from django.apps import AppConfig


class TempInheritappConfig(AppConfig):
    name = 'temp_inheritapp'
