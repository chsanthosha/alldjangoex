from django.contrib import admin
from django.views.generic import TemplateView

from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token,refresh_jwt_token,verify_jwt_token
from .views import RegisterView, VerifyEmailView, LoginView, LogoutView, UserDetailsView, PasswordChangeView, PasswordResetView, PasswordResetConfirmView

urlpatterns = [

    path('admin-create/',RegisterView.as_view()),
    path('verify-email/', VerifyEmailView.as_view()),

    # This url is used by django-allauth and empty TemplateView is
    # defined just to allow reverse() call inside app, for example when email
    # with verification link is being sent, then it's required to render email
    # content.

    # account_confirm_email - You should override this view to handle it in
    # your API client somehow and then, send post to /verify-email/ endpoint
    # with proper key.
    # If you don't want to use API on that step, then just use ConfirmEmailView
    # view from:
    # django-allauth https://github.com/pennersr/django-allauth/blob/master/allauth/account/views.py
    path('account-confirm-email/(?P<key>[-:\w]+)/', TemplateView.as_view()),
    path('admin-password/reset/', PasswordResetView.as_view()),
    path('admin-password/reset/confirm/', PasswordResetConfirmView.as_view()),
    path('admin-login/', LoginView.as_view()),
    # URLs that require a user to be logged in with a valid session / token.
    path('admin-logout/', LogoutView.as_view()),
    path('admin-user/', UserDetailsView.as_view()),
    path('admin-password/change/', PasswordChangeView.as_view()),
]



