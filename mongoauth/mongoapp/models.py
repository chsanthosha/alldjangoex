
# Create your models here.
from django.db import models
from allauth.account import app_settings as allauth_settings

from django.conf import settings

from rest_framework.authtoken.models import Token as DefaultTokenModel

from .utils import import_callable
from django.contrib.auth.models import AbstractUser

from django.utils.translation import ugettext_lazy as _
from django.conf import settings


# Register your models here.

TokenModel = import_callable(
    getattr(settings, 'REST_AUTH_TOKEN_MODEL', DefaultTokenModel))


from django.db import models
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager,UserManager
import datetime

class User(AbstractBaseUser):

        username = None
        name = models.CharField(max_length=100,blank=True, null=True)
        contact=models.IntegerField(blank=True, null=True)

        email = models.EmailField(_('email address'), unique=True)
        password = models.CharField(max_length=256)
        role = models.CharField(max_length=100,blank=True, null=True)
        status = models.CharField(max_length=100,blank=True, null=True)

        USERNAME_FIELD = 'email'
        REQUIRED_FIELDS = [ 'name','contact','password','role','status' ]

        def __str__(self):              # __unicode__ on Python 2
            return self.email

objects = UserManager()


class UserManager(BaseUserManager):

    use_in_migrations = True

    def create_user(self, name,contact,email, password,role,status):
        user = self.model(
            name=name,
            contact=contact,
            email=self.normalize_email(email),
            password=password,
            role=role,

            status=status



        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self,  name,contact,email, password,role):
        user = self.create_user(
            name=name,
            contact = contact,

            email=self.normalize_email(email),
            password=password,
            role=role

        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self,name,contact,email, password,role):
        user = self.create_user(
            contact=contact,

            email=self.normalize_email(email),
            password=password,
            role=role,
            name= "True",
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user