from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['name','contact','email','role']


admin.site.register(User,UserAdmin)