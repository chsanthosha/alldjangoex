from django.apps import AppConfig


class PdfrenderappConfig(AppConfig):
    name = 'pdfrenderapp'
