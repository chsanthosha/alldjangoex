import rest_auth
from rest_auth import registration
from django.urls import path,include
from rest_framework import routers
from .views import UserViewSet


router = routers.DefaultRouter()
router.register('users/', UserViewSet)

urlpatterns = [
    path('use/', include(router.urls)),
    path('auth/', include('rest_auth.urls')),
    path('auth/register/',include('rest_auth.registration.urls'))
]